# bot qui enregistre des livres que j'ai envie de lire qu va me servir de to do list, quand j'ai lu un livre j'enregistre une note.
# plusieurs utilisateurs peuvent noter des livres et peuvent enregistrer un livre, dire à mon robot de rentrer le nom d'un livre et de lui attribuer une note et si le livre contient déja une note il fait la moyenne des notes et affiche le numéro.

# A bot that records books I want to read, serving as a to-do list. When I read a book, I record a note.
# Multiple users can rate books and save a book. They can tell the bot to enter the name of a book and assign a note. 
# If the book already has a note, it averages the notes and displays the rating.

from discord.ext import commands
import discord
import json
import requests
import random 
from googletrans import Translator 
import signal

books_to_read = []  # Move the list outside the class

class MyClient(commands.Bot):
    def __init__(self, command_prefix, intents):
        super().__init__(command_prefix=command_prefix, intents=intents)

    async def on_ready(self):
        print(f'Logged on as {self.user}!')

    async def on_message(self, message):
        await self.process_commands(message)

# Discord.py configuration
intents = discord.Intents.default()
intents.message_content = True

# Add commands to the bot
client = MyClient(command_prefix='!', intents=intents)

# Load the list from the file (if the file exists)
try:
    with open('books.json', 'r') as file:
        books_to_read = json.load(file)
except FileNotFoundError:
    books_to_read = []

@commands.command(name='add_book', help='Adds a book to the to-read list with a note.')
async def add_book(ctx, *, args):
    global books_to_read

    # Use the split function to separate the book name and the note
    parts = args.split('"')
    book = parts[1].strip()  # The book name is between the quotes
    note = parts[2].strip() if len(parts) > 2 else ''  # The note is after the second set of quotes
    
    # Add the book to the list
    books_to_read.append({'book': book, 'note': note})

    await ctx.send(f'Book added to the list: {book}')

@commands.command(name='add_note', help='Adds a note to an existing book or creates a new note.')
async def add_note(ctx, book, note):
    global books_to_read

    # Search for the book in the list
    for item in books_to_read:
        if item['book'] == book:
            notes = item.get('notes', [])
            notes.append(float(note))
            item['notes'] = notes

            # Calculate the average of the notes
            average = sum(notes) / len(notes)

            await ctx.send(f'Note added for the book {book}: {note}')
            await ctx.send(f'Average notes for the book {book}: {average:.2f}')
            break
    else:
        await ctx.send(f'The book {book} was not found in the list. Add it first with the !add_book command.')

@commands.command(name='modify_book', help='Modifies the name and/or note of an existing book.')
async def modify_book(ctx, old_name, new_name='', new_note=''):
    for item in books_to_read:
        if item['book'] == old_name:
            if new_name:
                item['book'] = new_name
            if new_note:
                item['note'] = new_note
            save_list()
            await ctx.send(f'Book modified successfully. New name: {item["book"]}, New note: {item["note"]}')
            break
    else:
        await ctx.send(f'The book {old_name} was not found in the list.')

def save_list():
    with open('books.json', 'w') as file:
        json.dump(books_to_read, file)

@commands.command(name='delete_book', help='Deletes a book from the list.')
async def delete_book(ctx, book):
    global books_to_read

    for item in books_to_read:
        if item['book'] == book:
            books_to_read.remove(item)
            save_list()
            await ctx.send(f'Book deleted from the list: {book}')
            break
    else:
        await ctx.send(f'The book {book} was not found in the list.')

@commands.command(name='list_books', help='Displays the list of books to read with their notes.')
async def list_books(ctx):
    if not books_to_read:
        await ctx.send('The list is empty. Add books with the !add_book command.')
    else:
        list_books = '\n'.join([f'- {item["book"]} - rated: {item["note"]}/10' for item in books_to_read])
        await ctx.send(f'List of books to read:\n{list_books}')

@commands.command(name='add_random_book', help='Adds a random personal development book to the list.')
async def add_random_book(ctx):
    global books_to_read

    # Make a request to the Google Books API to get a random personal development book
    url = 'https://www.googleapis.com/books/v1/volumes?q=development+personal+growth'
    response = requests.get(url)

    if response.status_code == 200:
        data = response.json()
        if 'items' in data:
            random_book = random.choice(data['items'])['volumeInfo']['title']

            # Translate the title to French
            translator = Translator()
            translation = translator.translate(random_book, src='en', dest='fr')
            random_book_fr = translation.text

            # Add the book to the list
            books_to_read.append({'book': random_book_fr, 'note': ''})

            # Save the list to the JSON file
            save_list()

            await ctx.send(f'Book added: {random_book_fr}')
        else:
            await ctx.send('No personal development books found in the API response.')
    else:
        await ctx.send('Error getting data from the Google Books API.')

@commands.command(name='rate_random_book', help='Assigns a random note to a book from the list.')
async def rate_random_book(ctx):
    global books_to_read

    if not books_to_read:
        await ctx.send('The list is empty. Add books with the !add_book command.')
    else:
        chosen_book = random.choice(books_to_read)
        book_name = chosen_book['book']
        random_note = round(random.uniform(0, 10), 2)

        notes = chosen_book.get('notes', [])
        notes.append(random_note)
        chosen_book['notes'] = notes

        # Calculate the average of the notes
        average = sum(notes) / len(notes)

        # Translate the title to French
        translator = Translator()
        translation = translator.translate(book_name, src='fr', dest='en')
        book_in_english = translation.text

        await ctx.send(f'Random note added for the book {book_in_english}: {random_note}')
        await ctx.send(f'Average notes for the book {book_in_english}: {average:.2f}')

client.add_command(add_book)
client.add_command(add_note)
client.add_command(list_books)
client.add_command(modify_book)
client.add_command(delete_book)
client.add_command(add_random_book)
client.add_command(rate_random_book)

# Lancez le bot avec votre token
client.run('mettre sa clé token')
e